#ifndef SENSOR_H
#define SENSOR_H

#include "doors_status_data.h"

#include <stdint.h>

/*Door right sensor use PB3 pin*/
/*Door left sensor use PB4 pin*/
void sensor_init(void);

E_door_state get_right_door_state(void);
E_door_state get_left_door_state(void);
uint8_t sensors_get_sensors_state_change(void);
void sensors_reset_sensors_state_change(void);

#endif
