#include "uart.h"
#include "sensor.h"
#include "rf_transmitter.h"
#include "cpu_freq.h"
#include "doors_status_data.h"

#include <stdio.h>
#include <avr/io.h>
#include <avr/sleep.h>
#include <avr/interrupt.h>
#include <util/delay.h>

int main (void)
{
	uint8_t doors_status_message[E_DOORS_FRAME_SIZE];
	uint8_t frame_counter = 1;

	/* Set the clock to 8Mhz */
	cpu_freq_set_8Mhz_internal_clock();

	/* Init a software uart on pin PB1 (USI output) */
	UART_init(115200);

	/* Init the sensor module */
	sensor_init();

	/* Init wireless transmitter */
	rf_transmitter_init();

	/* Select the sleep mode we want (here: most power saving one) */
	set_sleep_mode(SLEEP_MODE_PWR_DOWN);

	UART_tx_str("Transmitter!\n\r");

	while(1)
	{
		/* If we wake up this means there was an interrupt for one of 
		 * the two doors sensor.
		 * We start by disabling the interrupt to avoid parasitic interrupt during
		 * the management of the previous one, for this we need to be sure that at 
		 * the end of the send sequence that the doors state is still the same, if
		 * not we need to keep sending update status until the status is stable.
		 * Before we go to sleep we make sure to enable the interrupt
		 * */

		UART_tx_str("Wake up!\n\r");
		_delay_ms(50);

		/* The process will go trought this loop 10 times and each time sleep for one seconde.
		 * If the sensors interrupt occured during one of the ten loop the loop will start over
		 * from 0.
		 * Each time we loop we check if the doors state as change, if yes we resend the data, if
		 * no we stay silent to avoid flooding the rf link.
		 * After the 10 ininterrupted loop the board will go to sleep */
		for(int send_counter = 0; send_counter < 5; send_counter++)
		{
			/* If sensors state change during the sending sequence
			 * we reset the counter to start sending again 
			 */
			if(sensors_get_sensors_state_change() > 0){
				send_counter = 0;
			}

			/* Initialize payload */
			doors_status_message[E_DOORS_MESSAGE_IDENTIFIER] = DOORS_MESSAGE_IDENTIFIER;

			/* Retrive doors status */
			doors_status_message[E_DOORS_RIGHT_STATUS] = get_right_door_state();
			doors_status_message[E_DOORS_LEFT_STATUS] = get_left_door_state();

			/* Add the actual frame counter to the message */
			doors_status_message[E_DOORS_FRAME_COUNTER] = frame_counter;

			/* Send state using 433Mhz */
			rf_transmitter_send_message(doors_status_message, sizeof(doors_status_message));

			/* Increment the frame counter */
			frame_counter++;

			/* Wait 2 sec before sending again */
			_delay_ms(2000);
		}

		UART_tx_str("Go to sleep\n\r");
		_delay_ms(50);

		/* Go to sleep mode */
		sleep_mode();
	}
}
