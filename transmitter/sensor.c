#include "sensor.h"
#include "uart.h"

#include <avr/io.h>
#include <avr/interrupt.h>

static volatile uint8_t sensors_state_change = 0;

/*Door right sensor use PB3 and left PB4 pin*/
void sensor_init(void){
	/* Set the Pin PB3 as Input (0)*/
	DDRB &= ~_BV(DDB3);
	PORTB &= ~_BV(PORTB3); // disable pull up

	/* Set the Pin PB4 as Input (0)*/
	DDRB &= ~_BV(DDB4);
	PORTB &= ~_BV(PORTB4); // disable pull up

	/* Enable pin change interrupt */
	GIMSK |= _BV(PCIE);

	/* Reset interrupt flag */
	GIFR |= _BV(PCIF);

	/* Set pin mask to trigger on PB3 and PB4 interrupt */
	PCMSK |= _BV(PCINT3);
	PCMSK |= _BV(PCINT4);

	/* Enable interrupt */
	sei();
}

/* Return the sensors state change flag value */
uint8_t sensors_get_sensors_state_change(void)
{
	uint8_t save_sensors_state_change;

	/* Disable interrupt */
	cli();

	/* Save value */
	save_sensors_state_change = sensors_state_change;

	/* Re enable interrupt */
	sei();

	return save_sensors_state_change;
}

/* Reset the sensor flag */
void sensors_reset_sensors_state_change(void)
{
	/* Disable interrupt */
	cli();

	/* Reset the flag */
	sensors_state_change = 0;

	/* Re enable interrupt */
	sei();
}

E_door_state get_right_door_state(void){
	if( PINB & _BV(PINB3)){
		return E_DOOR_OPEN;
	}else{
		return E_DOOR_CLOSE;
	}

	/* Useless */
	return E_DOOR_CLOSE;
}

E_door_state get_left_door_state(void){
	if( PINB & _BV(PINB4)){
		return E_DOOR_OPEN;
	}else{
		return E_DOOR_CLOSE;
	}

	/* Useless */
	return E_DOOR_CLOSE;
}

ISR(PCINT0_vect){
	/* Pin change interrupt handler */
	sensors_state_change = 1;
}
