#include "uart.h"
#include "rf_transmitter.h"
#include "cpu_freq.h"

#include <stdio.h>
#include <string.h>
#include <avr/io.h>
#include <avr/sleep.h>
#include <avr/interrupt.h>
#include <util/delay.h>

//#define DATA_SIZE 18
#define DATA_SIZE 8

int main (void)
{
	//uint8_t counter = 0;
	//char message[DATA_SIZE] = {0};
	uint8_t message[DATA_SIZE] = {65, 66, 67, 68, 69, 70, 71, 72};

	/* Set the clock to 8Mhz */
	cpu_freq_set_8Mhz_internal_clock();

	/* Init a software uart on pin PB1 (USI output) */
	UART_init();

	/* Init wireless transmitter */
	rf_transmitter_init();

	UART_tx_str("Transmitter!\n\r");

	while(1)
	{
		//memset(message, '\0', DATA_SIZE);
		//snprintf(message, DATA_SIZE, "Hello World %03d\n\r", counter);
		//counter++;

		/* Send message */
		///rf_transmitter_send_message((uint8_t *)message, DATA_SIZE);
		rf_transmitter_send_message((uint8_t *)message, DATA_SIZE);

		_delay_ms(5000);
	}
}
