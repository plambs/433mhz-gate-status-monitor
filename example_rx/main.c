#include "uart.h"
#include "rf_receiver.h"
#include "cpu_freq.h"

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <avr/power.h>
#include <util/delay.h>
#include <util/crc16.h>

static void receiver_data_callback(uint8_t *message, uint8_t message_length, E_crc_state crc_state){
	//UART_tx_str((char*)message);

	for(int offset=0; offset < message_length; offset++){
		UART_tx((char) message[offset]);
		_delay_us(1200);
	}

	if(crc_state == E_CRC_OK){
		UART_tx_str("CRC OK\n\r");
	}else{
		UART_tx_str("CRC NOK\n\r");
	}
}

int main (void)
{
	/* Set the clock to 8Mhz */
	cpu_freq_set_8Mhz_internal_clock();

	/* Init a software uart on pin PB1 (USI output) */
	UART_init();

	UART_tx_str("Receiver!\n\r");
	_delay_ms(50);

	/* PBO as debug */
	/* Init PBx as Output */
	DDRB |= _BV(DDB0);
	DDRB |= _BV(DDB3);
	DDRB |= _BV(DDB4);
	DDRB |= _BV(DDB5);

	/* Init the 433mhz receiver */
	rf_receiver_init(receiver_data_callback);


	/*Set pin Low*/
	PORTB &= ~_BV(PORT0);

	while(1)
	{
		/* Kick the receiver statemachine to manage the led */
		rf_receiver_kick_message_statemachine();
	}
}
