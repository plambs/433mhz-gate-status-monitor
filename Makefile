default : build_all

build_transmitter : 
	cd transmitter && $(MAKE)

build_receiver : 
	cd receiver && $(MAKE)

build_all : build_transmitter build_receiver
	@echo "Build all"

flash_transmitter:
	cd transmitter && $(MAKE) flash

flash_receiver:
	cd receiver && $(MAKE) flash

clean_transmitter:
	cd transmitter && $(MAKE) clean

clean_receiver:
	cd receiver && $(MAKE) clean

clean_all: clean_transmitter clean_receiver
	@echo "Clean all"

.PHONY: build_all flash_transmitter flash_receiver clean_all
