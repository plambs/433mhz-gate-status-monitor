#ifndef LED_MANAGER_H
#define LED_MANAGER_H

#include <stdint.h>

void led_manager_init_right_doors_led(void);
void led_manager_init_left_doors_led(void);

void led_manager_set_right_door_led_state(uint8_t state_to_set);
void led_manager_set_left_door_led_state(uint8_t state_to_set);

void led_manager_turn_off_all_door_state_led(void);

#endif
