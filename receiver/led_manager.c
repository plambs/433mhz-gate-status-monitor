#include "led_manager.h"

#include <avr/io.h>

/*
 * Init the right doors status led port (Pin PB3)
 * as output and with a default state to off.
 */
void led_manager_init_right_doors_led(void){
	DDRB |= _BV(DDB3);
	PORTB &= ~_BV(PORT3);
}

/*
 * Init the left doors status led port (Pin PB4)
 * as output and with a default state to off.
 */
void led_manager_init_left_doors_led(void){
	DDRB |= _BV(DDB4);
	PORTB &= ~_BV(PORT4);
}

/*
 * Set the new wanted state to the right doors status led.
 * If status is true (i.e: != 0) the led is on, else the
 * led is off.
 */
void led_manager_set_right_door_led_state(uint8_t state_to_set){
	if(state_to_set){
		PORTB |= _BV(PORT3);
	}else{
		PORTB &= ~_BV(PORT3);
	}
}

/*
 * Set the new wanted state to the ledt doors status led.
 * If status is true (i.e: != 0) the led is on, else the
 * led is off.
 */
void led_manager_set_left_door_led_state(uint8_t state_to_set){
	if(state_to_set){
		PORTB |= _BV(PORT4);
	}else{
		PORTB &= ~_BV(PORT4);
	}
}

/*
 * This function turn off all the doors led.
 */
void led_manager_turn_off_all_door_state_led(void)
{
	/* Turn off door right led */
	led_manager_set_right_door_led_state(0);

	/* Turn off door left led */
	led_manager_set_left_door_led_state(0);
}
