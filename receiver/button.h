#ifndef BUTTON_H
#define BUTTON_H

void button_init(void);
void button_disable_interrupt(void);
void button_enable_interrupt(void);

#endif
