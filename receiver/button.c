#include "button.h"
#include "led_manager.h"

#include <avr/io.h>
#include <avr/interrupt.h>

static volatile uint8_t portB_history;

void button_init(void)
{
	/* Init PB0 as input for the button that force the led off  */
	DDRB &= ~_BV(DDB0);
	PORTB &= ~_BV(PORT0);

	/* Enable pin change interrupt */
	GIMSK |= _BV(PCIE);

	/* Reset interrupt flag */
	GIFR |= _BV(PCIF);

	/* Set pin mask to trigger on PB0 interrupt */
	PCMSK |= _BV(PCINT0);

	/* Save history */
	portB_history = PINB;

	/* Enable interrupt */
	sei();
}

void button_disable_interrupt(void)
{
	/* Since we know that the only pin change interrupt we use is 
	 * for the pin PB0 we can disable PC interrupt entirely */

	/* Disable pin change interrupt */
	GIMSK &= ~_BV(PCIE);

	/* Reset interrupt flag */
	GIFR |= _BV(PCIF);

	/* Set pin mask to NOT trigger on PB0 interrupt */
	PCMSK &= ~_BV(PCINT0);
}

void button_enable_interrupt(void)
{
	/* Enable pin change interrupt */
	GIMSK |= _BV(PCIE);

	/* Reset interrupt flag */
	GIFR |= _BV(PCIF);

	/* Set pin mask to trigger on PB0 interrupt */
	PCMSK |= _BV(PCINT0);
}

ISR(PCINT0_vect){
	/* Check the rising edge of the button press (PB0),
	 * if the pin is HIGH, shutdown the led */
	uint8_t portB_change = portB_history ^ PINB;

	/* Save history */
	portB_history = PINB;

	if(portB_change & _BV(PINB0)){
		/* Interrupt append on PB0 */
		/* Read PB0 value, if pin is set -> turn of the led */
		if(PINB & _BV(PINB0)){
			/* Turn off all the door led */
			led_manager_turn_off_all_door_state_led();

			/* Disable the button interrupt to avoid parasitic call */
			button_disable_interrupt();
		}
	}
}
