#include "uart.h"
#include "rf_receiver.h"
#include "led_manager.h"
#include "cpu_freq.h"
#include "button.h"
#include "doors_status_data.h"

#include <stdio.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/power.h>
#include <util/delay.h>
#include <util/crc16.h>

static uint8_t last_received_framecounter = 0;
static uint16_t number_lost_message = 0;

static void receiver_data_callback(uint8_t *message, uint8_t message_length, E_crc_state crc_state){
	/* Check if the message_length match the one expected */
	if(message_length != E_DOORS_FRAME_SIZE){
		/* Doesn't match, skip this message */
		return;
	}

	/* Check the message identifier */
	if(message[E_DOORS_MESSAGE_IDENTIFIER] != DOORS_MESSAGE_IDENTIFIER){
		/* Doesn't match, skip this message */
		return;
	}

	/* Check the CRC */
	if(crc_state != E_CRC_OK){
		/* Doesn't match, skip this message */
		UART_tx_str("CRC NOK\n\r");
		return;
	}

	/* All check are done, now we can manage the doors state data */
	led_manager_set_right_door_led_state((message[E_DOORS_RIGHT_STATUS] == E_DOOR_OPEN) ? 1 : 0);

	led_manager_set_left_door_led_state((message[E_DOORS_LEFT_STATUS] == E_DOOR_OPEN) ? 1 : 0);

	/* Manage the frame counter data to measure the number of lost message */

	/* Add the diff between the last received framecounter on the actual received one minus one
	 * to the list message count
	 */
	number_lost_message += (uint8_t)(message[E_DOORS_FRAME_COUNTER] - last_received_framecounter) - 1;

	/* update the last received framecounter */
	last_received_framecounter = message[E_DOORS_FRAME_COUNTER];

	#define UART_MESSAGE_LENGTH 20
	char uart_message[UART_MESSAGE_LENGTH] = {'\0'};
	snprintf(uart_message, UART_MESSAGE_LENGTH, "lost msg: %d\n\r", number_lost_message);
	UART_tx_str(uart_message);
}

int main (void)
{
	/* Set the clock to 8Mhz */
	cpu_freq_set_8Mhz_internal_clock();

	/* Init a software uart on pin PB1 (USI output) */
	UART_init(115200);

	/* Init the 433mhz receiver */
	rf_receiver_init(receiver_data_callback);

	UART_tx_str("Receiver!\n\r");

	/* Init the button to reset the led */
	button_init();

	/* Init the two doors status led */
	led_manager_init_right_doors_led();
	led_manager_init_left_doors_led();

	while(1)
	{
		/* Kick the receiver statemachine to manage the led */
		rf_receiver_kick_message_statemachine();
	}
}
