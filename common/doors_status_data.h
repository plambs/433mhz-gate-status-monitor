#ifndef DOOR_STATUS_DATA_H
#define DOOR_STATUS_DATA_H

#define DOORS_MESSAGE_IDENTIFIER 0x55

typedef enum {
	E_DOOR_OPEN = 0x8E,
	E_DOOR_CLOSE = 0xE8,
} E_door_state;

typedef enum {
	E_DOORS_MESSAGE_IDENTIFIER = 0,
	E_DOORS_LEFT_STATUS = 1,
	E_DOORS_RIGHT_STATUS = 2,
	E_DOORS_FRAME_COUNTER = 3,
	E_DOORS_FRAME_SIZE = 4, /* This is used to define the buffer size */
} E_doors_data_offset;

#endif
