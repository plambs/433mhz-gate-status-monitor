#include "cpu_freq.h"

#include <avr/power.h>
#include <avr/interrupt.h>

/*
 * Raise the Cpu clock to 8Mhz using the factory attiny85 configuration.
 * This function will bump the 1Mhz (nominal/factory) clock to 8Mhz.
 */
void cpu_freq_set_8Mhz_internal_clock(void){
	/* Allow to change the clock using the CLKPS[3:0] bit */
	/* Reset CLKPS[3:0] to raise the clock to 8Mhz */

	/* Disable interrupts system wide */
	cli();

	/* Enable the Prescaler change */
	CLKPR = (1 << CLKPCE);

	/* Reset the Prescaler to go at 8Mhz */
	CLKPR = 0;

	/* Re enable the interrputs system wide */
	sei();
}
