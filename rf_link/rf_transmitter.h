#ifndef RF_TRANSMITTER_H
#define RF_TRANSMITTER_H

#include <stdint.h>

void rf_transmitter_init(void);
void rf_transmitter_send_byte(uint8_t data);
int rf_transmitter_send_message(uint8_t *message, uint8_t message_length);
void rf_transmitter_set_number_preamble(uint8_t nb_preamble);
void rf_transmitter_set_number_repeat(uint8_t nb_repeat);
//void wireless_send_doors_status(uint8_t status);


#endif
