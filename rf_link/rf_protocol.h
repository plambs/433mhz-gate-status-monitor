#ifndef RF_PROTOCOL_H
#define RF_PROTOCOL_H

#define DEFAULT_NUMBER_OF_PREAMBLE_BYTE 3 /* Number of preamble byte send before the start data byte, used to ajust the gain of the receiver */
#define DEFAULT_NUMBER_OF_MESSAGE_REPEAT 1 /* How much time should we repeat the message send to be sure the receiver received it */
#define DELAY_BETWEEN_REPEAT_IN_MS 100 /* ms */

#define HEADER_LENGTH 1 /* 1 Byte that give the data length */
#define RECEIVE_MESSAGE_BUFFER_SIZE 32 /* Message + Header */

/* TODO */
#define PREAMBLE_BYTE 0xFF /* As much one as possible */
#define START_HEADER_BYTE 0xE5
#define START_DATA_BYTE 0x5E

/*                                       |---------------------HEADER-----------------------|-------------------------MESSAGE-----------------------|*/
/*|--preamble--|--preamble--|--preamble--|--header-start-byte--|--data-size--|--header-crc--|--data-start-byte--|--data--|--data--.....|--data-crc--|*/
/*                                       |-------1 Byte--------|---1 Byte----|----1 Byte----|------1 Byte-------|----MAX 32 Bytes------|---1 Byte---|*/

/* Too little and the receiver can't manage the data
 * Too big and the gain of the receiver cranck up */
#define DELAY_BETWEEN_BYTE_MS 1 /*ms*/
#define DELAY_BETWEEN_BYTE_US 500 /*us*/

/* Choose the fast or slow version of the protocol */
#define SLOW_PROTOCOL 1

#ifdef SLOW_PROTOCOL

/* Slow protocol */
#define HIGH_PULSE_DURATION 200 /*us*/

#define GLOBAL_START_PULSE_DURATION 700 /*us*/
#define GLOBAL_ONE_PULSE_DURATION   600 /*us*/
#define GLOBAL_ZERO_PULSE_DURATION  500 /*us*/

#define START_PULSE_LOW_DURATION (GLOBAL_START_PULSE_DURATION - HIGH_PULSE_DURATION) /* 500 us*/
#define ONE_PULSE_LOW_DURATION   (GLOBAL_ONE_PULSE_DURATION - HIGH_PULSE_DURATION)   /* 400  us*/
#define ZERO_PULSE_LOW_DURATION  (GLOBAL_ZERO_PULSE_DURATION - HIGH_PULSE_DURATION)  /* 300  us*/

#define START_PULSE_MAX_DETECTION_THRESHOLD (GLOBAL_START_PULSE_DURATION + (HIGH_PULSE_DURATION / 2)) /* 1000 us*/
#define START_PULSE_DETECTION_THRESHOLD     (GLOBAL_ONE_PULSE_DURATION + ((GLOBAL_START_PULSE_DURATION - GLOBAL_ONE_PULSE_DURATION) / 2))  /* 650 us*/
#define ONE_PULSE_DETECTION_THRESHOLD       (GLOBAL_ZERO_PULSE_DURATION + ((GLOBAL_ONE_PULSE_DURATION - GLOBAL_ZERO_PULSE_DURATION) / 2))  /* 550 us*/
/* Zero is everything under the ONE_PULSE_DETECTION_THRESHOLD value  */
#define MINIMUM_VALID_PULSE_DURATION        (ZERO_PULSE_LOW_DURATION / 2) /* This is the minimum pulse to be registred to be considered valid and not noise in us */

#else

/* Fast protocol */
#define HIGH_PULSE_DURATION 80 /*us*/

#define GLOBAL_START_PULSE_DURATION 250 /*us*/
#define GLOBAL_ONE_PULSE_DURATION   150 /*us*/
#define GLOBAL_ZERO_PULSE_DURATION  100 /*us*/

#define START_PULSE_LOW_DURATION 170 //(GLOBAL_START_PULSE_DURATION - HIGH_PULSE_DURATION) /*170 us*/
#define ONE_PULSE_LOW_DURATION   70  //(GLOBAL_ONE_PULSE_DURATION - HIGH_PULSE_DURATION)   /*70  us*/
#define ZERO_PULSE_LOW_DURATION  20  //(GLOBAL_ZERO_PULSE_DURATION - HIGH_PULSE_DURATION)  /*20  us*/

#define START_PULSE_MAX_DETECTION_THRESHOLD 300 /*us*/
#define START_PULSE_DETECTION_THRESHOLD 200 /*us*/
#define ONE_PULSE_DETECTION_THRESHOLD 125 /*us*/
/* Zero is everything under that 125us value  */
#define MINIMUM_VALID_PULSE_DURATION        (ZERO_PULSE_LOW_DURATION / 2) /* This is the minimum pulse to be registred to be considered valid and not noise in us */

#endif /*SLOW_PROTOCOL*/

#endif
