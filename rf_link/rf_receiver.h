#ifndef RF_RECEIVER_H
#define RF_RECEIVER_H

#include <rf_protocol.h>

#include <stdint.h>

typedef enum{
	E_CRC_NOK = 0,
	E_CRC_OK = 1,
}E_crc_state;

typedef void (*rf_receiver_callback)(uint8_t *message, uint8_t message_length, E_crc_state crc_state);

/*
 * State of the messages state machine,
 * That state machine is responsible to check if we
 * receive the correct sequence of byte, like preamble ->
 * data start byte -> data and crc at the end.
 * If everything is correct the statemachine call the callback with
 * data.
 */
typedef enum{
	E_MESSAGE_STATE_INIT = 0,
	E_MESSAGE_STATE_PREAMBLE = 1,
	//E_MESSAGE_STATE_HEADER_START = 2,
	E_MESSAGE_STATE_RECEIVE_DATA_LENGTH = 3,
	E_MESSAGE_STATE_CHECK_HEADER_CRC = 4,
	E_MESSAGE_STATE_DATA_START = 5,
	E_MESSAGE_STATE_RECEIVE_MESSAGE_BYTE = 6,
	E_MESSAGE_STATE_RECEIVE_MESSAGE_CRC = 7,
	//E_MESSAGE_STATE_CHECK_MESSAGE_CRC = 8,
	E_MESSAGE_STATE_CALL_MESSAGE_CALLBACK = 9,
}E_message_state;

/*
 * Message statemachine data struct
 */
typedef struct {
	E_message_state state;
	rf_receiver_callback data_callback_handler;
	uint8_t message_buffer[RECEIVE_MESSAGE_BUFFER_SIZE];
	uint8_t data_length;
	uint8_t data_buffer_offset;
	uint8_t crc;
	uint8_t receive_crc;
} T_message_statemachine;

/*
 * State of the byte statemachine, this statemachine is responsible
 * of the reception of the byte, starting with a start pulse and followed by the
 * 8 data bits.
 * The receive bits are put in one byte and send to the messages statemachine.
 */
typedef enum {
	E_RECEIVE_STATE_SEARCH_START_PULSE = 0,
	E_RECEIVE_STATE_RECEIVE_BITS = 1,
	E_RECEIVE_STATE_MANAGE_FULL_BYTE = 2,
} E_byte_statemachine_state;

/*
 * Byte statemachine data struct
 */
typedef struct {
	E_byte_statemachine_state state;
	uint8_t tmp_byte_buffer;
	uint8_t bit_offset;
	uint8_t last_full_byte;

	/* Timer interrupt data */
	volatile uint16_t saved_timer_value;
	volatile uint8_t timer_overflow;
} T_byte_statemachine;

int rf_receiver_init(rf_receiver_callback callback);
void rf_receiver_kick_message_statemachine(void);
void rf_receiver_kick_byte_statemachine(void);
uint8_t get_last_received_byte(void);
uint8_t get_door_status(void);

#endif
