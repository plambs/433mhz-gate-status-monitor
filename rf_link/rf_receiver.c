#include "rf_receiver.h"
#include "rf_protocol.h"

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <util/crc16.h>
#include <stddef.h>

/* Global data for the two statemachine */
static T_message_statemachine message_statemachine;
static T_byte_statemachine byte_statemachine;

int rf_receiver_init(rf_receiver_callback callback){
	if(callback == NULL)
	{
		/* Error callback is null */
		return -1;
	}

	/* Init PB2 as Input */
	DDRB &= ~_BV(DDB2);
	PORTB &= ~_BV(PORTB2);

	/* Enable external interrupt */
	GIMSK |= _BV(INT0);

	/* trigger the interrupt on falling edge ISC01 -> 1 and ISC00-> 0 */
	MCUCR |= (~_BV(ISC00) | _BV(ISC01));

	/* Reset interrupt flag */
	GIFR |= _BV(INTF0);

	/* Set pin mask to trigger on PB2 interrupt */
	PCMSK |= _BV(PCINT2);

	/* Init timer1 */
	/* No prescaler */
	//TCCR1 |= _BV(CS10);
	//TCCR1 |= _BV(CS11); //CS11 is CK/2
	//TCCR1 |= (_BV(CS12) | _BV(CS10)); // CK/16
	TCCR1 |= (_BV(CS12) | _BV(CS11)); // CK/32

	/**
	 * CK = 8Mhz
	 * If prescalar is 8 -> 1 tick timer == 1us
	 * Prescalar set to 32 => timer overflow will happen after ~1sec
	 * and 1 timer tick is 4us (32/8 == 4)
	 */

	/* Enable timer1 overflow interrupt */
	TIMSK |= _BV(TOIE1);

	/* Timer value is in TCNT1 */

	/* Save the callback handler */
	message_statemachine.data_callback_handler = callback;

	/* Init starting statemachine state */
	byte_statemachine.state = E_RECEIVE_STATE_SEARCH_START_PULSE;
	message_statemachine.state = E_MESSAGE_STATE_INIT;

	/* Enable the interrupt */
	sei();

	return 0;
}

void rf_receiver_kick_message_statemachine(void)
{
	uint8_t last_byte;

	/* Move statemachine forward if needed */
	rf_receiver_kick_byte_statemachine();

	last_byte = get_last_received_byte();

	if((last_byte == 0) && (message_statemachine.state != E_MESSAGE_STATE_CALL_MESSAGE_CALLBACK)){
		/* No byte to manage */
		return;
	}


	/* Manange the last byte received if needed */
	switch(message_statemachine.state){
		case E_MESSAGE_STATE_INIT:
			if(last_byte == PREAMBLE_BYTE){
				message_statemachine.state = E_MESSAGE_STATE_PREAMBLE;
			}
			break;
		case E_MESSAGE_STATE_PREAMBLE:
			if(last_byte == PREAMBLE_BYTE){
				/* Nothing todo actually, ignore the byte and wait the data start byte */
				message_statemachine.state = E_MESSAGE_STATE_PREAMBLE;
			}else if(last_byte == START_HEADER_BYTE){
				/* Update header CRC */
				message_statemachine.crc = 0;
				message_statemachine.crc = _crc8_ccitt_update(message_statemachine.crc, last_byte);

				/* Next byte is data length */
				message_statemachine.state = E_MESSAGE_STATE_RECEIVE_DATA_LENGTH;
			}else{
				/* Should not happend */
				message_statemachine.state = E_MESSAGE_STATE_INIT;
			}
			break;
		case E_MESSAGE_STATE_RECEIVE_DATA_LENGTH:
				/* Update header CRC */
				message_statemachine.crc = _crc8_ccitt_update(message_statemachine.crc, last_byte);

				/* Save the data length received */
				message_statemachine.data_length = last_byte;

				/* Jump to the next state, E_MESSAGE_STATE_CHECK_CRC */
				message_statemachine.state = E_MESSAGE_STATE_CHECK_HEADER_CRC;
			break;
		case E_MESSAGE_STATE_CHECK_HEADER_CRC:
			if(message_statemachine.crc == last_byte){
				/* Header CRC is correct, we can continue */
				message_statemachine.state = E_MESSAGE_STATE_DATA_START;
			}else{
				/* Header CRC is not correct, ignore the data and restart the statemachine */
				message_statemachine.state = E_MESSAGE_STATE_INIT;
			}
			break;
		case E_MESSAGE_STATE_DATA_START:
			if(last_byte == START_DATA_BYTE){
				/* Compute CRC */
				message_statemachine.crc = 0;
				message_statemachine.crc = _crc8_ccitt_update(message_statemachine.crc, last_byte);

				/* Next byte is the data, jump to data received */
				message_statemachine.state = E_MESSAGE_STATE_RECEIVE_MESSAGE_BYTE;

				/* Reset the buffer data offset */
				message_statemachine.data_buffer_offset = 0;
			}else{
				/* Should not happend */
				message_statemachine.state = E_MESSAGE_STATE_INIT;
			}
			break;
		case E_MESSAGE_STATE_RECEIVE_MESSAGE_BYTE:
			/* Get each byte until data_length reached
			 * Recompute the CRC at each byte
			 */
			message_statemachine.message_buffer[message_statemachine.data_buffer_offset] = last_byte;
			message_statemachine.data_buffer_offset++;

			/* Update CRC */
			message_statemachine.crc = _crc8_ccitt_update(message_statemachine.crc, last_byte);

			/* Check if the end is reached */
			if(message_statemachine.data_buffer_offset >= message_statemachine.data_length){
				/*Jump to the next state*/
				message_statemachine.state = E_MESSAGE_STATE_RECEIVE_MESSAGE_CRC;
			}
			break;
		case E_MESSAGE_STATE_RECEIVE_MESSAGE_CRC:
			/* Remember received crc */
			message_statemachine.receive_crc = last_byte;

			/* Jump to the next state */
			message_statemachine.state = E_MESSAGE_STATE_CALL_MESSAGE_CALLBACK;
			break;
		case E_MESSAGE_STATE_CALL_MESSAGE_CALLBACK:
			/* Call the message data callback with the CRC state */
			message_statemachine.data_callback_handler(message_statemachine.message_buffer,
			                                           message_statemachine.data_length,
			                                          (message_statemachine.crc == message_statemachine.receive_crc) ? E_CRC_OK : E_CRC_NOK);

			/* Reset the statemachine */
			message_statemachine.state = E_MESSAGE_STATE_INIT;
			break;
		default:
			/*Should not be here, reset statemachine */
			message_statemachine.state = E_MESSAGE_STATE_INIT;
			break;
	}
}

void rf_receiver_kick_byte_statemachine(void)
{
	uint16_t volatile local_saved_timer_value;

	/* Critical section start */
	/* Save the last saved_timer_value in a local variable to use in this function */
	cli();
	local_saved_timer_value = byte_statemachine.saved_timer_value;
	sei();
	/* Reset the saved timer value to avoid rerun of the statemachine */
	cli();
	byte_statemachine.saved_timer_value = 0;
	sei();
	/* Critical section end */

	/* Manange timer overflow */
	if(byte_statemachine.timer_overflow){
		byte_statemachine.state = E_RECEIVE_STATE_SEARCH_START_PULSE;
		byte_statemachine.timer_overflow = 0;
	}


	/* If no falling edge detected or the state is NOT E_RECEIVE_STATE_SEARCH_START_PULSE
	 * we simply exit since there is nothing to do */
	if((local_saved_timer_value == 0) && (byte_statemachine.state != E_RECEIVE_STATE_MANAGE_FULL_BYTE)){
		/* Nothing todo */
		return;
	}

	/* We have some new falling edge */
	switch(byte_statemachine.state)
	{
		case E_RECEIVE_STATE_SEARCH_START_PULSE:
			/* Search for Start pulse */
			if ((local_saved_timer_value > START_PULSE_DETECTION_THRESHOLD) && (local_saved_timer_value < START_PULSE_MAX_DETECTION_THRESHOLD)){
				/* Set the start pulse received flag */
				byte_statemachine.state = E_RECEIVE_STATE_RECEIVE_BITS;
				byte_statemachine.timer_overflow = 0;
				byte_statemachine.bit_offset = 0;
			}
			break;

		case E_RECEIVE_STATE_RECEIVE_BITS:
			/* Receive all the bit */
			//if (local_saved_timer_value > START_PULSE_DETECTION_THRESHOLD){
			if ((local_saved_timer_value > START_PULSE_DETECTION_THRESHOLD) && (local_saved_timer_value < START_PULSE_MAX_DETECTION_THRESHOLD)){
				/* New startpulse detected, flush the last one*/
				byte_statemachine.bit_offset = 0;
			} else if ((local_saved_timer_value > ONE_PULSE_DETECTION_THRESHOLD) && (local_saved_timer_value <= START_PULSE_DETECTION_THRESHOLD)){
				/* One received */
				byte_statemachine.tmp_byte_buffer |= (1 << byte_statemachine.bit_offset);
				byte_statemachine.bit_offset++;
			} else if (local_saved_timer_value < ONE_PULSE_DETECTION_THRESHOLD){
				/* Zero received */
				byte_statemachine.tmp_byte_buffer &= ~(1 << byte_statemachine.bit_offset);
				byte_statemachine.bit_offset++;
			}

			if(byte_statemachine.bit_offset >= 8){
				/* Go to the next statemachine state */
				byte_statemachine.state = E_RECEIVE_STATE_MANAGE_FULL_BYTE;
			}
			break;

		case E_RECEIVE_STATE_MANAGE_FULL_BYTE:
			/* Manage the complete byte */
			byte_statemachine.last_full_byte = byte_statemachine.tmp_byte_buffer;
			byte_statemachine.state = E_RECEIVE_STATE_SEARCH_START_PULSE;
		break;

		default:
			/* Should not be here, reset the statemachine */
			byte_statemachine.state = E_RECEIVE_STATE_SEARCH_START_PULSE;
		break;
	}
}

uint8_t get_last_received_byte(void)
{
	uint8_t last_byte = byte_statemachine.last_full_byte;
	byte_statemachine.last_full_byte = 0;

	return last_byte;
}

ISR(INT0_vect){
	/* Falling edge of the 433Mhz module output data line detected */
	/* Save the timer value and reset it */
	uint16_t measured_time = TCNT1 * 4;

	/* Reset timer counter */
	TCNT1 = 0;

	// Manage minimum noise interrupt
	if(measured_time < MINIMUM_VALID_PULSE_DURATION){
		/* Pulse is to short to be registred
		 * When the pulse is too short if we don't register the pulse time
		 * this avoid unescessary work from the transport state machine
		 */
		return;
	}

	//saved_timer_value = TCNT1 * 2; // prescallar of 16 (8 * 2)
	//saved_timer_value = TCNT1 * 4; // prescallar of 32 (8 * 4)
	byte_statemachine.saved_timer_value = measured_time;
}

ISR(TIMER1_OVF_vect){ /* each 1024us if no timer reset */
	/* Timer overflow reached */
	byte_statemachine.saved_timer_value = 0;
	byte_statemachine.timer_overflow = 1;
}
