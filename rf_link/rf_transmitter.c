#include "rf_transmitter.h"
#include "rf_protocol.h"

#include <avr/io.h>
#include <util/delay.h>
#include <util/crc16.h>

/*
 * Rf link send configuration
 * Can be twiked by calling function
 */
static uint8_t config_number_of_message_repeat = DEFAULT_NUMBER_OF_MESSAGE_REPEAT;
static uint8_t config_number_of_preamble_byte = DEFAULT_NUMBER_OF_PREAMBLE_BYTE;

void rf_transmitter_init(void)
{
	/* Init PB2 as Output */
	DDRB |= _BV(DDB2);

	/*Set pin Low*/
	PORTB &= ~_BV(PORT2);
}

void rf_transmitter_send_byte(uint8_t data)
{
	/* Wait delay before sending start pulse
	 * This create time for the receiver to process data
	 * */
	PORTB &= ~_BV(PORT2);

	#ifdef SLOW_PROTOCOL
		_delay_ms(DELAY_BETWEEN_BYTE_MS);
	#else
		_delay_us(DELAY_BETWEEN_BYTE_US);
	#endif

	/* Send start pulse */
	/* Set pin high for HIGH_PULSE_DURATION */
	PORTB |= _BV(PORTB2);
	_delay_us(HIGH_PULSE_DURATION);
	//_delay_ms(10);

	/* Set pin low for the START_PULSE_LOW_DURATION */
	PORTB &= ~_BV(PORT2);
	_delay_us(START_PULSE_LOW_DURATION);

	/* Set pin high for the HIGH_PULSE_DURATION */
	PORTB |= _BV(PORTB2);
	_delay_us(HIGH_PULSE_DURATION);

	/* Send data bit per bit */
	//for(int i = 8; i > 0; i--){
	for(int i = 0; i < 8; i++){
		if(data & (1 << i)){
			/* Bit is one in the data */
			/* Set pin low for the ONE_PULSE_LOW_DURATION */
			PORTB &= ~_BV(PORT2);
			_delay_us(ONE_PULSE_LOW_DURATION);

		} else {
			/* Bit is zero in the data */
			/* Set pin low for the ZERO_PULSE_LOW_DURATION */
			PORTB &= ~_BV(PORT2);
			_delay_us(ZERO_PULSE_LOW_DURATION);
		}

		/* Set the pin high for HIGH_PULSE_DURATION us*/
		PORTB |= _BV(PORTB2);
		_delay_us(HIGH_PULSE_DURATION);
	}
	/* Set the pin to low to stop transmitting */
	PORTB &= ~_BV(PORT2);
}

int rf_transmitter_send_message(uint8_t *message, uint8_t message_length){
	uint8_t crc = 0; /* crc is data_start byte + data byte */

	/* Check if the message is longer than the receive buffer */
	if(message_length > RECEIVE_MESSAGE_BUFFER_SIZE){
		/* Error: message to long */
		return -1;
	}

	/* TODO: compute message and header crc one time for all */

	/* Send the whole data sequence as many time as requested */
	for(int repeat = 0; repeat < config_number_of_message_repeat; repeat++){
		/* Reset CRC */
		crc = 0;

		/*Send as much preamble as configured*/
		for(int i = 0; i < config_number_of_preamble_byte; i++){
			rf_transmitter_send_byte(PREAMBLE_BYTE);
		}

		/* HEADER*/

		/* Send start header byte */
		crc = _crc8_ccitt_update(crc, START_HEADER_BYTE);
		rf_transmitter_send_byte(START_HEADER_BYTE);

		/* Send data length byte */
		crc = _crc8_ccitt_update(crc, message_length);
		rf_transmitter_send_byte(message_length);

		/* Send header CRC byte */
		rf_transmitter_send_byte(crc);

		/* MESSAGE */

		/* Reset crc */
		crc = 0;

		/*Send the start of data byte*/
		crc = _crc8_ccitt_update(crc, START_DATA_BYTE);
		rf_transmitter_send_byte(START_DATA_BYTE);

		/*Send the two doors status*/
		for(uint8_t offset = 0; offset < message_length; offset++){
			/* Update crc value with all data byte */
			crc = _crc8_ccitt_update(crc, message[offset]);

			/* Send data byte */
			rf_transmitter_send_byte(message[offset]);
		}

		/*Send message CRC byte*/
		rf_transmitter_send_byte(crc);

		/* Wait a little before sending next data except if the nb_repeat is set to one */
		if(config_number_of_message_repeat > 1){
			_delay_ms(DELAY_BETWEEN_REPEAT_IN_MS);
		}
	}

	return 0;
}

void rf_transmitter_set_number_preamble(uint8_t nb_preamble)
{
	config_number_of_preamble_byte = nb_preamble;
}

void rf_transmitter_set_number_repeat(uint8_t nb_repeat)
{
	config_number_of_message_repeat = nb_repeat;
}
